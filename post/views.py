from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, FormView, UpdateView, CreateView
from post.models import Post, Comments
from post.forms import PostForm


class Posts(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'post/post_list.html'
    paginate_by = 10

    def get_queryset(self):
        qs = Post.objects.filter(is_delete=False).order_by('-created_at')
        if not self.request.user.is_authenticated():
            return qs.exclude(is_private=True)
        return qs


class PostsIndex(Posts):
    template_name = 'post/posts_index.html'

    def get_queryset(self):
        return super(PostsIndex, self).get_queryset().exclude(rating__lt=10)


class PostDetail(DetailView):
    model = Post
    template_name = 'post/post-detail.html'

    def get_object(self):
        """
        Для неавторизованного пользователя возвращает 404 ошибку
        можно также использовать декоратор login_required
        """
        object = super(PostDetail, self).get_object()
        if not self.request.user.is_authenticated():
            raise Http404
        return object

    def get_context_data(self, **kwargs):
        context = super(PostDetail, self).get_context_data(**kwargs)
        context['comments'] = Comments.objects.filter(post=self.object).order_by('-date')
        return context


class PrivatePostList(ListView):
    model = Post
    paginate_by = 10
    context_object_name = 'posts'
    template_name = 'post/private_posts.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """
        Декорируем диспетчер функцией login_required,
        чтобы запретить просмотр отображения неавторизованными
        пользователями
        """
        return super(PrivatePostList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        """
        Список объектов будет состоять лишь из приватных и не удаленных статей
        """
        return Post.objects.filter(is_private=True, is_delete=False)


class CreatePost(CreateView):
    form_class = PostForm
    success_url = '/'
    template_name = 'post/create_post.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreatePost, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        #instanse = form.save(commit=False)
        #instanse.author = request.user
        #instanse.save()
        Post.objects.create(**form.cleaned_data)
        return redirect(self.get_success_url())


class PostUpdate(UpdateView):
    form_class = PostForm
    model = Post
    template_name = 'post/create_post.html'
    success_url = '/'

