from django.conf.urls import include, url
from post.views import PostsIndex, Posts, PostDetail, CreatePost, PostUpdate


urlpatterns = [
    url(r'^page(?P<page>\d+)/$', PostsIndex.as_view()),

    url(r'^posts/page(?P<page>\d+)/$', Posts.as_view()),

    url(r'^posts/(?P<slug>[-\w]+)/edit/$', PostUpdate.as_view(), name='post-update'),
    url(r'^posts/(?P<slug>[-\w]+)/$', PostDetail.as_view(), name='post-detail'),
    url(r'^posts/create/$', CreatePost.as_view(), name='post-create'),
    url(r'^posts/$', Posts.as_view()),

    url(r'^(?P<slug>[-\w]+)/$', PostDetail.as_view(), name='post-detail'),
    url(r'^$', PostsIndex.as_view(), name='index'),

]

