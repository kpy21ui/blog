from django.contrib import admin
from post.models import Post, Comments, Photo


class PostInline(admin.StackedInline):
    model = Photo
    extra = 3


class PostAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'created_at')
    prepopulated_fields = {"slug": ("name",)}
    inlines = [PostInline]


class CommentsAdmin(admin.ModelAdmin):
    exclude = ['date']


class PhotoAdmin(admin.ModelAdmin):
    exclude = ['post']
    list_display = ['post', 'thumbnail']


admin.site.register(Post, PostAdmin)
admin.site.register(Comments, CommentsAdmin)
admin.site.register(Photo, PhotoAdmin)