from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models


class User(AbstractUser):
    followers = models.ManyToManyField('self', related_name='followees', symmetrical=False)

    def get_absolute_url(self):
        return reverse('author-detail', kwargs={'pk': self.pk})


class Post(models.Model):
    author = models.ForeignKey(User, verbose_name="Автор")
    name = models.CharField(max_length=250, verbose_name='Название')
    slug = models.SlugField(max_length=250, verbose_name='Слуг')
    text = models.TextField(verbose_name='Текст')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    rating = models.IntegerField(default=0, verbose_name='Рейтинг')
    is_private = models.BooleanField(default=False, verbose_name='Приватная статья?')
    is_delete = models.BooleanField(default=False, verbose_name='Удаленная статья?')

    def __str__(self):
        return self.name

    class Meta():
        db_table = 'posts'
        ordering = ['-created_at', ]
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class Comments(models.Model):
    owner = models.ForeignKey(User, verbose_name='Владелец')
    post = models.ForeignKey(Post, verbose_name='Комментарии')
    text = models.TextField(verbose_name='Текст')
    date = models.DateField(auto_now_add=True, verbose_name='Дата публикации')

    def __str__(self):
        return self.text

    class Meta():
        db_table = 'comments'
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'


class Photo(models.Model):
    post = models.ForeignKey(Post, verbose_name='Статья')
    image = models.ImageField(upload_to='images/posts/%Y/%m/%d')

    def thumbnail(self):
        return """<a href="/media/%s"><img border="0" alt="" src="/media/%s" height="40" /></a>""" \
               % (self.image.name, self.image.name)
    thumbnail.allow_tags = True

